#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#define MAX 4
#define MAX2 8
#define MAX3 30

void shell_sort(int vetor[], int tamanho) {

    int j, valor;
    int gap = 1;

    do {
        gap = 3 * gap + 1;
    } while (gap < tamanho);

    do {
        gap = gap / 3;
        for (int i = gap; i < tamanho; i++) {
            valor = vetor[i];
            j = i - gap;

            while (j >= 0 && valor < vetor[j]) {
                vetor[j + gap] = vetor[j];
                j = j - gap;
            }

            vetor[j + gap] = valor;
        }
    } while (gap > 1);


}

void imprimir_desordenado(int vetor[], int tamanho){
    printf("Exibi os elementos do vetor n�o ordenado\n");
    for(int i = 0; i < tamanho; i++) printf("%d\t", vetor[i]);
    printf("\n");
}

void imprimir_ordenado(int vetor[], int tamanho){
    printf("Exibi os elementos do vetor n�o ordenado\n");
    shell_sort(vetor, tamanho);
    for(int i = 0; i < tamanho; i++) printf("%d\t", vetor[i]);
    printf("\n");
}

int main() {

    setlocale(LC_ALL, "Portuguese");

    int vetor1[MAX] = {4, 2, 3, 1};
    imprimir_desordenado(vetor1, MAX);
    imprimir_ordenado(vetor1, MAX);

    int vetor2[MAX2] = {8, -2, 4, 1, 3, 5, 7, 6};
    imprimir_desordenado(vetor2, MAX2);
    imprimir_ordenado(vetor2, MAX2);

    int vetor3[MAX3] = {8, -2, 4, 1, 3, 5, 7, 6, 50, 40,
                        -10, 4, 16, 51, 42, 14, 28, 18, 22, 25,
                        9, 23, 21, 11, 13, 35, 28, 33, 31, 29};

    imprimir_desordenado(vetor3, MAX3);
    imprimir_ordenado(vetor3, MAX3);


    return 0;
}
